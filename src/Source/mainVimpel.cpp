//============================================================================
// Name        : Vimpel.cpp
// Author      : Mark
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//#include "../Header/FIO.h"
#include "../Header/DataFormat.h"
#include "../Header/test.h"


int main()
{

	DataProcess init;
	/*
	 *  Чтение бинарника (подключить FIO.h )
	 */
//	DataFormat dataMas[15];
//
//	for (int i = 0; i < 15; i++)
//	{
//		dataMas[i].time = init.getRandom();
//		init.genData(dataMas[i]);
//		dataMas[i].hashSumData = init.getSum(dataMas[i]);
//	}
//
//
//	DataFormat dataRead[15];
//	BinIO<DataFormat> fio;
//
//	for (int i = 0; i < 15; ++i)
//	{
//		fio.writeFile(dataMas[i],"dataWrite.bin", ios::app);
//	}
//
//	fio.readFile(dataRead, "dataWrite.bin");


	/*
	 * 	Чтение из txt
	 */

	TxtIO<DataFormat> txt;
	int size = 4;
	DataFormat *rawData =  new DataFormat[size];
	txt.readFile(rawData,"dataTXT.txt", size);
	init.dispData(rawData, size);

	txt.convertToBin(rawData, size, "dataBin.bin");

	BinIO<DataFormat> bin;
	DataFormat *processData =  new DataFormat[size];
	bin.readFile(processData, "dataBin.bin", size);
	init.dispData(processData, size);

	delete[] rawData;
	delete[] processData;

	return 0;
}
