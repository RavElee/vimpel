/*
 * DataFormat.cpp
 *
 *  Created on: 23 мар. 2020 г.
 *      Author: User
 */

#include "../Header/DataFormat.h"

unsigned DataProcess::getSum(DataFormat& obj)
{
	unsigned temp = 0;
	for(int i  = 0; i < 6; i++)
		temp += obj.data[i];
	return hash(temp);
}

//Вихрь Мерсенна
unsigned DataProcess::getRandom()
{
	rand();

	return rand();
}

void DataProcess::genData(DataFormat& obj)
{
//	srand(time(0)); // устанавливаем значение системных часов в качестве стартового числа
//	rand();
	for (int i = 0; i < 6; i++)
	{
		obj.data[i] = getRandom() % 32;
	}
}

void DataProcess::dispData(DataFormat obj[], int size)
{
	for(int i = 0; i < size; i++)
	{
		std::cout << obj[i].time << "\t";
		for(int j = 0; j < 6; j++)
			std::cout << obj[i].data[j] << "\t";
		std::cout << obj[i].hashSumData << "\n";
	}
}
