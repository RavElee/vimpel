/*
 * DataFormat.h
 *
 *  Created on: 23 мар. 2020 г.
 *      Author: User
 */

#ifndef HEADER_DATAFORMAT_H_
#define HEADER_DATAFORMAT_H_
#include <iostream>
#include <cstdlib>
#include <ctime>


struct DataFormat
{
	int time;
	unsigned int data[6];
	unsigned int hashSumData;
	DataFormat(): time(0), hashSumData(0)
	{
		for(int i = 0; i < 6; i ++)
			data[i] = 0;
	}
};


class DataProcess
{
public:
	DataProcess(){srand(time(0));}
	unsigned getSum(DataFormat& obj);
	unsigned getRandom();
	void genData(DataFormat& obj);
	void dispData(DataFormat obj[], int size);
private:
	std::hash<unsigned int> hash;
};


#endif /* HEADER_DATAFORMAT_H_ */
