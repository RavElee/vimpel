/*
 * test.h
 *
 *  Created on: 9 апр. 2020 г.
 *      Author: User
 */



#include <string>
#include <fstream>
#include <ostream>
#include <cstdio>

using namespace std;

template<class T>
class FIO
{
public:
	virtual void readFile(T& data, const string name) = 0;
	virtual void writeFile(T& data, const string name, ios::openmode mode) = 0;
	virtual void readFile(T* data, const string name, int size) = 0;
	virtual ~FIO() {};
protected:
	ofstream fout;
	ifstream fin;
};


template<class T>
class BinIO : public FIO<T>
{
public:
	void readFile(T& data,  const string name);
	void readFile(T* data, const string name, int size = 0);
	void writeFile(T& data, const string name,  ios::openmode mode = ios::out);
private:

};


template<class T>
class TxtIO : public FIO<T>
{
public:
	void readFile(T& data,  const string name);
	virtual void writeFile(T& data, const string name, ios::openmode mode){};
	void readFile(DataFormat data[],  const string name, int size = 0);
	void convertToBin(DataFormat in[],  int size,  const string name);
};

/*
 *  read from binary file
 *  (data - one element)
 */
template<class T>
void BinIO<T>::
readFile(T& data,  const string name)
{
	FIO<T>::fin.open(name);
	if (!FIO<T>::fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;
		FIO<T>::fin.read((char*)&data, sizeof(T));
	}
	FIO<T>::fin.close();
}

/*
 *  read from binary file
 *  (data - array of element)
 */
template<class T>
void BinIO<T>::
readFile(T* data, const string name , int size)
{
	FIO<T>::fin.open(name);
	if (!FIO<T>::fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;
		T temp;
		int i = 0;
		while(FIO<T>::fin.read((char*)&temp, sizeof(T)))
		{
			data[i] = temp;
			i++;
		}
	}
	FIO<T>::fin.close();
	cout << "File is close (after read bin)" << endl;
}

/*
 *  write in binary file
 */
template<class T>
void BinIO<T>::
writeFile(T& data,  const string name,  ios::openmode mode)
{
	FIO<T>::fout.open(name, mode); // ofstream::app - дозаписать в конец файла
	if (!FIO<T>::fout.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for write" << endl;
		FIO<T>::fout.write((char*)&data, sizeof(T));
	}
	FIO<T>::fout.close();
	cout << "File is close (after write bin)" << endl;
}


/*
 *  read from txt file
 *  (data - one element)
 */
template<class T>
void TxtIO<T>::
readFile(T& data,  const string name)
{
	FIO<T>::fin.open(name);
	if (!FIO<T>::fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;
		FIO<T>::fin.read((char*)&data, sizeof(T));
	}
	FIO<T>::fin.close();
}

/*
 *  read from txt file
 *  (data - array of element)
 */
template<class T>
void TxtIO<T>::
readFile(DataFormat data[], const string name, int size)
{
	FIO<T>::fin.open(name);
	if (!FIO<T>::fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;

		if (size == 0)
		{
			unsigned int it = 0;

			while (FIO<T>::fin >> data[it].time >> data[it].hashSumData)
			{
				 for (int i = 0; i < 6; i ++)
				 {
					 FIO<T>::fin >> data[it].data[i];
				 }
				 it++;
			}
		}
		else
		{
			for(int j = 0; j < size; j++)
			{
				 FIO<T>::fin >> data[j].time >> data[j].hashSumData;
				 for (int i = 0; i < 6; i ++)
					 FIO<T>::fin >> data[j].data[i];
			}
		}
	}
	FIO<T>::fin.close();
	cout << "File is close (after read txt)" << endl;

}


template<class T>
void TxtIO<T>::
convertToBin(DataFormat in[], int size,  const string name)
{
	BinIO<DataFormat> temp;
	ios::openmode mode = ios::app;

	if(remove(name.c_str())) // удаление файла, если он есть
		std::cout << "Ошибка удаления файла" << endl;
	for (int i = 0; i < size; ++i)
		temp.writeFile(in[i], name, mode);

}


 /* HEADER_TEST_H_ */
