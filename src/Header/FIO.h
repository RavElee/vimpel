/*
 * Utility.h
 *
 *  Created on: 23 мар. 2020 г.
 *      Author: User
 */

#ifndef HEADER_FIO_H_
#define HEADER_FIO_H_

#include <fstream>
#include <iostream>

using namespace std;

// File input/output
template<class T>
class FIO
{
public:
	void writeFile(T& data, const char* nameFile);
	void readFile(T& data, const char* nameFile);
	void readFile(T* data, const char* nameFile);

private:
	ofstream fout;
	ifstream fin;
};

template<class T>
void FIO<T>::writeFile(T& data, const char* nameFile)
{
	fout.open(nameFile, ofstream::app);
	if (!fout.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for write" << endl;
		fout.write((char*)&data, sizeof(T));
	}
	fout.close();
}

template<class T>
void FIO<T>::readFile(T& data, const char* nameFile)
{
	fin.open(nameFile);
	if (!fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;
		fin.read((char*)&data, sizeof(T));
	}
	fin.close();
}

template<class T>
void FIO<T>::readFile(T* data, const char* nameFile)
{
	fin.open(nameFile);
	if (!fin.is_open())
	{
		cout << "Error opening file" << endl;
	}
	else
	{
		cout << "File is open for read" << endl;
		T temp;
		int i = 0;
		while(fin.read((char*)&temp, sizeof(T)))
		{
			data[i] = temp;
			i++;
		}
	}
	fin.close();
}

#endif /* HEADER_FIO_H_ */
